#include "widget.h"
#include "ui_widget.h"
#include <QFileDialog>
#include <QPixmap>
#include <QDebug>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    client = new QNetworkAccessManager(this);
    connect(client, &QNetworkAccessManager::finished,
            this, &Widget::handle_reply);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_openButton_clicked()
{
    QString filename = QFileDialog::getOpenFileName();
    image.setFileName(filename);
    image.open(QIODevice::ReadOnly);

    QUrl url("http://images.bemfa.com/upload/v1/upimages.php");
    QNetworkRequest request(url);
    request.setRawHeader("Content-Type", "image/jpg");
    request.setRawHeader("Authorization", "4e78c607dbbc9f4d6b5c16ffa5622fb7");
    request.setRawHeader("Authtopic", "images");

    client->post(request, &image);
}

void Widget::handle_reply(QNetworkReply* reply)
{
    qDebug() << reply->readAll();
    reply->deleteLater();
    image.close();
}

