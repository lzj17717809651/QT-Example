﻿#ifndef BAIDUSPEECH_H
#define BAIDUSPEECH_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QByteArray>

class BaiduSpeech : public QObject
{
    Q_OBJECT
public:
    explicit BaiduSpeech(QObject *parent);
    void setToken(const QString& token) {_token = token;}
    void doASR(const QByteArray data);
    void doTTS(const QString data);
    QString text();
    QByteArray audio();

signals:
    void doneASR(int error);
    void doneTTS(int error);

public slots:
    void on_ASRReply(QNetworkReply* reply);
    void on_TTSReply(QNetworkReply* reply);

private:
    QNetworkAccessManager* asr_client;
    QNetworkAccessManager* tts_client;
    QString _token;
    QString _text;
    QByteArray _audio;
};

#endif // BAIDUSPEECH_H
