#include "widget.h"
#include "ui_widget.h"
#include <QUrlQuery>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    uid = "4e78c607dbbc9f4d6b5c16ffa5622fb7";

    client = new QTcpSocket(this);
    connect(client, SIGNAL(connected()), this, SLOT(subscribe()));
    connect(client, SIGNAL(readyRead()), this, SLOT(process_msg()));
    client->connectToHost("bemfa.com", 8344);

    keepalive = new QTimer(this);
    connect(keepalive, SIGNAL(timeout()), this, SLOT(ping()));
    keepalive->start(10000);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::subscribe()
{
    qDebug() << "server connected";
    QUrlQuery query;
    query.addQueryItem("cmd", "1");
    query.addQueryItem("uid", uid);
    query.addQueryItem("topic", "client");
    qDebug() << query.toString();
    QString msg = query.toString() + "\r\n";
    client->write(msg.toUtf8());
}

void Widget::process_msg()
{
    if (!client->canReadLine())
    {
        //没有收完继续等待
        return;
    }
    QString msg = client->readLine();
    QUrlQuery query(msg.trimmed());
    for (auto& param:query.queryItems())
    {
        qDebug() << "key: " << param.first;
        qDebug() << "value: " << param.second;

        if (param.first == "msg")
        {
            ui->label->setText(param.second);
        }
    }
}

void Widget::ping()
{
    client->write("ping\r\n");
}

void Widget::on_pushButton_clicked()
{
    QUrlQuery query;
    query.addQueryItem("cmd", "2");
    query.addQueryItem("uid", uid);
    query.addQueryItem("topic", "test");
    query.addQueryItem("msg", ui->lineEdit->text());
    qDebug() << query.toString();
    QString msg = query.toString() + "\r\n";
    client->write(msg.toUtf8());
}
