﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QSerialPort>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_openButton_clicked();
    void on_readyRead();

    void on_sendButton_clicked();

private:
    Ui::Widget *ui;
    QSerialPort *port;
};
#endif // WIDGET_H
