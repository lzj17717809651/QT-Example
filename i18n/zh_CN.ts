<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>MainWindow</name>
    <message>
        <source>MainWindow</source>
        <translation type="vanished">主窗口</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="32"/>
        <source>Please choose language</source>
        <translation>请选择语言</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="55"/>
        <source>&amp;File</source>
        <translation>文件(&amp;F)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="70"/>
        <source>&amp;Edit</source>
        <translation>编辑(&amp;E)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="78"/>
        <source>&amp;Language</source>
        <translation>语言(&amp;L)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="99"/>
        <source>&amp;Open</source>
        <translation>打开(&amp;O)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="104"/>
        <source>&amp;Close</source>
        <translation>关闭(&amp;C)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="113"/>
        <source>&amp;New</source>
        <translation>新建(&amp;N)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="124"/>
        <source>Cu&amp;t</source>
        <translation>剪切(&amp;T)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="132"/>
        <source>&amp;Copy</source>
        <translation>复制(&amp;C)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="140"/>
        <source>&amp;Paste</source>
        <translation>粘贴(&amp;P)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="148"/>
        <source>&amp;Save</source>
        <translation>保存(&amp;S)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="156"/>
        <source>&amp;Chinese</source>
        <translation>中文(&amp;C)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="161"/>
        <source>&amp;English</source>
        <translation>&amp;English</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="166"/>
        <source>&amp;Japanese</source>
        <translation type="unfinished">日本語(&amp;J)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Notepad</source>
        <translation>记事本</translation>
    </message>
</context>
</TS>
