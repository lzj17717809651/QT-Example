﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    translator = new QTranslator(this);
    if (translator->load(":/i18n/" + QLocale::system().name() + ".qm"))
    {
        qApp->installTranslator(translator);
    }
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionEnglish_triggered()
{
    reTranslate("en_US");
}

void MainWindow::on_actionChinese_triggered()
{
    reTranslate("zh_CN");
}

void MainWindow::on_actionJapanese_triggered()
{
    reTranslate("ja_JP");
}

void MainWindow::reTranslate(const QString& lang)
{
    if (lang == "en_US")
    {
        qApp->removeTranslator(translator);
    }
    else
    {
        if (translator->load(":/i18n/" + lang + ".qm"))
        {
            qApp->installTranslator(translator);
        }
    }
    ui->retranslateUi(this);
}
