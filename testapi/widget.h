#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

#include <QNetworkAccessManager>
#include <QNetworkReply>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_reply(QNetworkReply* reply);

    void on_openButton_clicked();

    void on_browseButton_clicked();

    void on_requestButton_clicked();

private:
    Ui::Widget *ui;
    QNetworkAccessManager *client;
};
#endif // WIDGET_H
