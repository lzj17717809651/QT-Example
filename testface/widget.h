#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <opencv2/opencv.hpp>
#include <QTimer>
#include "baiduface.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();
    void detectFace(cv::Mat& img);

private slots:
    void updatePicture();
    void faceFound();

private:
    Ui::Widget *ui;
    cv::VideoCapture video;
    QTimer* timer;
    cv::Ptr<cv::CascadeClassifier> classifier;
    BaiduFace* bdf;
    QString groups;
};
#endif // WIDGET_H
