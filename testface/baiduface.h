#ifndef BAIDUFACE_H
#define BAIDUFACE_H

#include <QObject>
#include <QNetworkAccessManager>

class BaiduFace : public QObject
{
    Q_OBJECT
public:
    explicit BaiduFace(const QString& appkey, const QString& secret, QObject *parent = nullptr);
    void getToken();
    void search(const QByteArray face, const QString groups);
    QString userid();

signals:
    void found();

public slots:
    void on_tokenReply(QNetworkReply* reply);
    void on_searchReply(QNetworkReply* reply);

private:
    QNetworkAccessManager* _client;
    QString _appkey;
    QString _secret;
    QString _token;
    QString _uid;
};

#endif // BAIDUFACE_H
