#include "widget.h"
#include "ui_widget.h"
#include <QDragEnterEvent>
#include <QMimeData>
#include <QFile>
#include <QDebug>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    //对主窗口启用拖放事件
    setAcceptDrops(true);
    //禁用文本控件默认的拖放事件
    ui->textEdit->setAcceptDrops(false);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::dragEnterEvent(QDragEnterEvent *event)
{
    //拖入内容是否为地址
    if (event->mimeData()->hasUrls())
    {
        //允许拖入事件
        event->acceptProposedAction();
    }
}

//松开鼠标处理拖入的内容
void Widget::dropEvent(QDropEvent *event)
{
    //将地址转为本地路径
    QString filename = event->mimeData()->urls().first().toLocalFile();
    if (filename.isEmpty())
    {
        return;
    }
    qDebug() << filename;
    QFile file(filename);
    if (file.open(QIODevice::ReadOnly))
    {
        ui->textEdit->setText(file.readAll());
        file.close();
    }
}
